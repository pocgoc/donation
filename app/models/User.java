package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

@Entity
public class User extends Model
{
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public boolean usaCitizen;
	
	@OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
	List<Donation> donations = new ArrayList<Donation>();

	public User(String firstName, String lastName, String email, String password, boolean usaCitizen)
	{
		this.firstName = firstName;
		this.lastName  = lastName;
		this.email     = email;
		this.password  = password;
		this.usaCitizen = usaCitizen;
	}
	
	public static User findByEmail(String email)
	{
		return find("email", email).first();
	}

	public boolean checkPassword(String password)
	{
		return this.password.equals(password);
	}
}
