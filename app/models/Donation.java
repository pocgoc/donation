package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import play.db.jpa.Model;

@Entity
public class Donation extends Model
{

	@ManyToOne
	public User from;
	
	public String methodDonated;
	public long amountDonated;
	

   public Donation(User from, String methodDonated, long amountDonated)
   {
	this.from = from;
	this.methodDonated  = methodDonated;
	this.amountDonated  = amountDonated;
    }
}
