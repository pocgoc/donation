package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }

  public static void index()
  {
    render();
  }
  
  public static User currentlyLoggedIn()
  {
    User user = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    }
    else
    {
      index();
    }
    return user;
  }
  
  public static void register(String firstName, String lastName, String email, String password,  boolean usaCitizen)
  {
    Logger.info(firstName + " " + lastName + " " + email + " " + password);
    
    User user = new User(firstName, lastName, email, password, usaCitizen);
    user.save();
    
    index();
  }

  
  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      Welcome.index();

    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
}